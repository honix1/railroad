﻿using System.Collections.Generic;

namespace Railroad
{
    public class Bank
    {
        private readonly Dictionary<IBankUser, float> cashAccounts = 
            new Dictionary<IBankUser, float>();

        public Bank ()
        {

        }

        public float GetCash(IBankUser bankUser)
        {
            return cashAccounts.GetOrDefault(bankUser);
        }

        public void SetCash(IBankUser bankUser, float quant)
        {
            cashAccounts[bankUser] = quant;

            (bankUser as IBankNotify)?.CashChanged(quant, quant);
        }

        public float UpdateCash(IBankUser bankUser, float quant)
        {
            SetCash(bankUser, GetCash(bankUser) + quant);
            float sum = GetCash(bankUser);

            (bankUser as IBankNotify)?.CashChanged(quant, sum);

            return sum;
        }
    }

}