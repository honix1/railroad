﻿namespace Railroad
{
    public class BankOfAccount
    {
        private readonly Bank bank;
        private readonly IBankUser bankUser;

        public BankOfAccount
        (
            Bank bank,
            IBankUser bankUser
        )
        {
            this.bank = bank;
            this.bankUser = bankUser;
        }

        public float GetCash() => bank.GetCash(bankUser);
        public void SetCash(float quant) => bank.SetCash(bankUser, quant);
        public float UpdateCash(float quant) => bank.UpdateCash(bankUser, quant);
    }

}