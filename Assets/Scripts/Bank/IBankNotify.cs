﻿namespace Railroad
{
    public interface IBankNotify
    {
        void CashChanged(float delta, float sum);
    }

}