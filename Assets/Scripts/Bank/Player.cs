﻿using System;

namespace Railroad
{
    public class Player : IBankUser, IBankNotify
    {
        public event Action<float> SetCashEvent;

        public string Name { get; private set; } = "Player";

        public void CashChanged(float delta, float sum)
        {
            SetCashEvent(sum);
        }
    }
}