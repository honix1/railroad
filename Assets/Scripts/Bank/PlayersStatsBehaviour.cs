﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Railroad
{
    public class PlayersStatsBehaviour : MonoBehaviour
    {
        [SerializeField]
        private Text cashText;

        private Player player;

        [Inject]
        public void Initialize(Player player)
        {
            this.player = player;

            player.SetCashEvent += SetCash;
        }

        public void SetCash(float sum)
        {
            cashText.text = $"{Mathf.FloorToInt(sum)}₽";
        }
    }
}