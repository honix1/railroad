﻿using System.Collections.Generic;
using System.Linq;

namespace Railroad
{
    public class DirectedGraph<T> : IDirectedGraph<T> where T : class
    {

        private readonly Dictionary<T, DirectedGraphNode<T>> nodes =
            new Dictionary<T, DirectedGraphNode<T>>();

        private readonly List<DirectedGraphConnection<T>> connections =
            new List<DirectedGraphConnection<T>>();

        public IReadOnlyCollection<DirectedGraphConnection<T>> ReadConnections() => connections;

        public void AddConnection(T from, bool fromDir, T to, bool toDir)
        {
            connections.Add(new DirectedGraphConnection<T>()
            {
                from = AddNode(from),
                fromDir = fromDir,
                to = AddNode(to),
                toDir = toDir,
            });
        }

        private DirectedGraphNode<T> AddNode(T value)
        {
            if (nodes.ContainsKey(value))
            {
                return nodes[value];
            }
            else
            {
                DirectedGraphNode<T> node = new DirectedGraphNode<T>()
                {
                    value = value
                };
                nodes[value] = node;
                return node;
            }
        }

        public IEnumerable<DirectedGraphRouteNode<T>> Route(DirectedGraphNode<T> from, DirectedGraphNode<T> to)
        {
            ResetVisited();
            return
                RouteInner(from, to, new LinkedList<DirectedGraphRouteNode<T>>(), true) ??
                RouteInner(from, to, new LinkedList<DirectedGraphRouteNode<T>>(), false);
        }

        private void ResetVisited()
        {
            foreach (DirectedGraphNode<T> node in nodes.Values)
            {
                node.visited = false;
            }
        }

        // TODO: Naive implementation. Rewrite
        private IEnumerable<DirectedGraphRouteNode<T>> RouteInner(
            DirectedGraphNode<T> from,
            DirectedGraphNode<T> to,
            LinkedList<DirectedGraphRouteNode<T>> route,
            bool dir)
        {
            if (route.Count > 64)
            {
                // loop breakage
                return null;
            }

            if (from.Equals(to))
            {
                route.AddFirst(
                    new DirectedGraphRouteNode<T>() { 
                        value = to.value, 
                        direction = dir 
                    });
                return route.Reverse();
            }
            else
            {
                from.visited = true;

                foreach (DirectedGraphConnection<T> connection in
                    ConnectedNodes(from, dir).Where(x => !x.to.visited))
                {
                    var nextRoute = new LinkedList<DirectedGraphRouteNode<T>>(route);
                    nextRoute.AddFirst(
                        new DirectedGraphRouteNode<T>() { 
                            value = connection.from.value, 
                            direction = dir
                        });

                    IEnumerable<DirectedGraphRouteNode<T>> rec =
                        RouteInner(connection.to, to, nextRoute, connection.toDir);

                    if (rec != null)
                    {
                        return rec;
                    }
                }

                return null;
            }
        }

        private IEnumerable<DirectedGraphConnection<T>> ConnectedNodes(
            DirectedGraphNode<T> node,
            bool dir)
        {
            return connections
                .Where(x => x.from == node && x.fromDir == dir);
        }

        public IEnumerable<DirectedGraphRouteNode<T>> RouteByValue(T from, T to)
        {
            return Route(nodes[from], nodes[to]);
        }

        public int RemoveConnection(T value)
        {
            return connections
                .RemoveAll(x => HasLoad(x, value));
        }

        private static bool HasLoad(DirectedGraphConnection<T> connection, T value)
        {
            return
                connection.from.value == value ||
                connection.to.value == value;
        }

        public IEnumerable<DirectedGraphConnection<T>> GetToConnections(T value)
        {
            return connections
                .Where(x => x.from.value == value && x.fromDir == true);
        }

        public IEnumerable<DirectedGraphConnection<T>> GetFromConnections(T value)
        {
            return connections
                .Where(x => x.to.value == value && x.toDir == true);
        }
    }
}
