﻿namespace Railroad
{
    public class DirectedGraphConnection<T>
    {
        public DirectedGraphNode<T> from;
        public bool fromDir;
        public DirectedGraphNode<T> to;
        public bool toDir;
        //public float cost;
    }
}
