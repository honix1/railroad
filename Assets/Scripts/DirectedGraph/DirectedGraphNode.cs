﻿namespace Railroad
{
    public class DirectedGraphNode<T>
    {
        public T value;
        public bool visited;

        public override string ToString()
        {
            return $"Connection: value = {value}, visited = {visited}";
        }
    }
}
