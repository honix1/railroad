﻿namespace Railroad
{
    public class DirectedGraphRouteNode<T>
    {
        public T value;
        public bool direction;
    }
}
