﻿using System.Collections.Generic;

namespace Railroad
{
    public interface IDirectedGraph<T> where T : class
    {
        void AddConnection(T from, bool fromDir, T to, bool toDir);
        IEnumerable<DirectedGraphConnection<T>> GetFromConnections(T value);
        IEnumerable<DirectedGraphConnection<T>> GetToConnections(T value);
        IReadOnlyCollection<DirectedGraphConnection<T>> ReadConnections();
        int RemoveConnection(T value);
        IEnumerable<DirectedGraphRouteNode<T>> Route(DirectedGraphNode<T> from, DirectedGraphNode<T> to);
        IEnumerable<DirectedGraphRouteNode<T>> RouteByValue(T from, T to);
    }
}