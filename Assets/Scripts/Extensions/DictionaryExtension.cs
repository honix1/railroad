﻿using System.Collections.Generic;

namespace Railroad
{
    static public class DictionaryExtension
    {
        static public G GetOrDefault<T,G>(this Dictionary<T, G> dict, T key)
        {
            if (dict.TryGetValue(key, out var value))
            {
                return value;
            }
            return default;
        }
    }
}