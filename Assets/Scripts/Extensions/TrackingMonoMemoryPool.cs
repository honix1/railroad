﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class TrackingMonoMemoryPool<T> : MonoMemoryPool<T> where T : Component
    {
        private readonly HashSet<T> spawnedItems = new HashSet<T>();
        private bool despawning = false;

        protected override void OnSpawned(T item)
        {
            base.OnSpawned(item);
            spawnedItems.Add(item);
        }

        protected override void OnDespawned(T item)
        {
            base.OnDespawned(item);
            if (!despawning)
                spawnedItems.Remove(item);
        }

        public void DespawnAll()
        {
            despawning = true;
            foreach (var item in spawnedItems)
            {
                Despawn(item);
            }
            despawning = false;

            spawnedItems.Clear();
        }
    }
}