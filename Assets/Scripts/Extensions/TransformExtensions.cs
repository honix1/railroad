﻿using UnityEngine;

namespace Railroad
{
    static public class TransformExtensions
    {
        static public void SetMatrix(this Transform transform, Matrix4x4 matrix)
        {
            transform.SetPositionAndRotation(
                matrix.GetColumn(3),
                matrix.rotation
            );
        }
    }
}