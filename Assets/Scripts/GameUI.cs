using UnityEngine;
using Zenject;

namespace Railroad
{
    public class GameUI : MonoBehaviour
    {
        private RailroadInputBehaviour railroadControlsInput;

        [Inject]
        public void Initialize(RailroadInputBehaviour railroadControlsInput)
        {
            this.railroadControlsInput = railroadControlsInput;
        }

        public void AddRailsButton()
        {
            railroadControlsInput.SetAddRailsMode();
        }

        public void RemoveRailsButton()
        {
            railroadControlsInput.SetRemoveRailsMode();
        }

        //public void TrainsButton()
        //{
        //    railroadControlsInput.SetTrainsMode();
        //}

        public void AddStationMode()
        {
            railroadControlsInput.AddStationMode();
        }
    }
}