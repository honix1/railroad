﻿using System;
using UnityEngine;

namespace Railroad
{
    [Serializable]
    public class Good
    {
        public string id = "good";
        public string name = "Good";
        public float price = 1f;

        public Sprite icon;
        public RailcarBehaviour railcarPrefab;
    }
}