﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class GoodsManager : IGoodsManager, ITickable
    {
        private readonly Dictionary<object, Dictionary<Good, float>> objectToGoods = 
            new Dictionary<object, Dictionary<Good, float>>();
        private readonly Dictionary<object, List<GoodQuant>> objectToDelta =
            new Dictionary<object, List<GoodQuant>>();
        private readonly Dictionary<object, List<Good>> objectToNeeds = 
            new Dictionary<object, List<Good>>();

        private readonly GoodsSettings goodsSettings;

        public GoodsManager(GoodsSettings goodsSettings)
        {
            this.goodsSettings = goodsSettings;
        }

        public void AddGoods(
            object obj,
            Good good,
            float quant,
            GoodsNotifyMode notifyMode = GoodsNotifyMode.All)
        {
            var dict =
                objectToGoods.GetOrDefault(obj) ??
                (objectToGoods[obj] = new Dictionary<Good, float>());

            dict[good] = dict.GetOrDefault(good) + quant;

            switch (notifyMode)
            {
                case GoodsNotifyMode.All:
                    (obj as IGoodsNotify)?.GoodsChanged(good, quant);
                    break;
                case GoodsNotifyMode.Whole:
                    int diff = Mathf.FloorToInt(dict[good]) - Mathf.FloorToInt(dict[good] - quant);
                    if (diff != 0)
                    {
                        (obj as IGoodsNotify)?.GoodsChanged(good, diff);
                    }
                    break;
            }
        }

        public void AddNeeds(object obj, Good good)
        {
            var list =
                objectToNeeds.GetOrDefault(obj) ??
                (objectToNeeds[obj] = new List<Good>());

            list.Add(good);
        }

        // TODO: rename to SetDelta, and implement overwrite
        public void AddDelta(object obj, Good good, float delta)
        {
            var list =
                objectToDelta.GetOrDefault(obj) ??
                (objectToDelta[obj] = new List<GoodQuant>());

            list.Add(new GoodQuant() { good = good, quant = delta });
        }

        public void Tick()
        {
            foreach(var goodDeltasKeyValue in objectToDelta)
            {
                var obj = goodDeltasKeyValue.Key;
                var goodDeltas = goodDeltasKeyValue.Value;

                foreach (var goodDelta in goodDeltas)
                {
                    AddGoods(obj, goodDelta.good, goodDelta.quant * Time.deltaTime, GoodsNotifyMode.Whole);
                }
            }
        }

        public IEnumerable<GoodQuant> GetGoods(object obj)
        {
            if (objectToGoods.TryGetValue(obj, out var value))
            {
                return value.Select(x => new GoodQuant() { good = x.Key, quant = x.Value });
            }
            return Enumerable.Empty<GoodQuant>();
        }

        // TODO: maybe remove latter
        // but toList is errorless when we iterate in this list (copy) and modify objectToGoods
        public IReadOnlyCollection<KeyValuePair<Good, float>> GetGoodsList(object obj)
        {
            if (objectToGoods.TryGetValue(obj, out var value))
            {
                return value.ToList();
            }
            return Array.Empty<KeyValuePair<Good, float>>();
        }

        public IEnumerable<Good> GetNeeds(object obj)
        {
            if (objectToNeeds.TryGetValue(obj, out var value))
            {
                return value;//.GetEnumerator();
            }
            return Enumerable.Empty<Good>();
        }

        public bool IsNeeds(object obj, Good good)
        {
            return GetNeeds(obj).Any(x => x == good);
        }

        public float GetGoodsCount(object obj, Good good)
        {
            var apples = GetGoods(obj).Where(x => x.good == good).FirstOrDefault();
            return apples.quant;
        }

        public Good GetGood(string id)
        {
            return goodsSettings.Goods.Where(x => x.id == id).FirstOrDefault();
        }

        // Temporary
        public Good GetApples()
        {
            return GetGood("apple");
        }

        // Temporary
        public float GetApplesCount(object obj)
        {
            return GetGoodsCount(obj, GetApples());
        }
    }
}
