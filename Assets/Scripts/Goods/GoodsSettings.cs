﻿using System.Collections.Generic;
using UnityEngine;

namespace Railroad
{
    [CreateAssetMenu(fileName = "GoodsSettings", menuName = "Settings/GoodsSettings")]
    public class GoodsSettings : ScriptableObject
    {
        [field: SerializeField]
        public List<Good> Goods { get; private set; }
    }
}