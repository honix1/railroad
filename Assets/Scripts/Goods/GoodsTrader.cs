﻿using UnityEngine;

namespace Railroad
{
    public class GoodsTrader
    {
        private readonly GoodsManager goodsManager;
        private readonly BankOfAccount bankOfAccount;

        public GoodsTrader
        (
            GoodsManager goodsManager,
            BankOfAccount bankOfAccount
        )
        {
            this.goodsManager = goodsManager;
            this.bankOfAccount = bankOfAccount;
        }

        public bool TrySellAll(ITrader from, ITrader to)
        {
            bool any = false;

            foreach (var sellGood in goodsManager.GetGoodsList(from))
            {
                any |= TrySell(from, to, sellGood.Key);
            }

            return any;
        }

        public bool TrySell(ITrader from, ITrader to, Good good)
        {
            var have = goodsManager.GetGoodsCount(from, good);

            if (have <= 0)
                return false;
            if (!goodsManager.IsNeeds(to, good))
                return false;

            goodsManager.AddGoods(from, good, -1);
            goodsManager.AddGoods(to, good, 1);

            // TODO: Update back account of whom?
            bankOfAccount.UpdateCash(good.price);

            Debug.Log($"\"{from.Name}\" sells 1 {good.name} to \"{to.Name}\"");

            return true;
        }
    }
}