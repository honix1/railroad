﻿using System.Collections.Generic;

namespace Railroad
{
    public interface IGoodsManager
    {
        void AddGoods(object obj, Good good, float quant, GoodsNotifyMode notifyMode);
        Good GetApples();
        float GetApplesCount(object obj);
        IEnumerable<GoodQuant> GetGoods(object obj);
        IReadOnlyCollection<KeyValuePair<Good, float>> GetGoodsList(object obj);
    }
}