﻿namespace Railroad
{
    internal interface IGoodsNotify
    {
        void GoodsChanged(Good good, float change);
    }
}