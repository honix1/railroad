﻿namespace Railroad
{
    public interface ITrader
    {
        string Name { get; }
    }
}