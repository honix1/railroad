﻿using System;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class BankInstaller : Installer<BankInstaller>
    {
        [Serializable]
        public class Settings
        {
            public PlayersStatsBehaviour playersStatsPrefab;
            public Transform playersStatsParentTransform;
        }

        private readonly Settings settings;

        public BankInstaller(Settings settings)
        {
            this.settings = settings;
        }

        public override void InstallBindings()
        {
            Container.Bind<BankOfAccount>()
                .AsSingle();

            Container.Bind<Bank>()
                .AsSingle();

            Container.BindInterfacesAndSelfTo<Player>()
                .AsSingle();

            Container.Bind<PlayersStatsBehaviour>()
                .FromComponentInNewPrefab(settings.playersStatsPrefab)
                .UnderTransform(settings.playersStatsParentTransform)
                .AsSingle()
                .NonLazy();
        }
    }
}