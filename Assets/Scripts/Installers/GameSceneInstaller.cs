using UnityEngine;
using Zenject;

namespace Railroad
{
    public class GameSceneInstaller : MonoInstaller
    {
        [SerializeField]
        private StationLabelsInstaller.Settings stationLabelSettings;        
        [SerializeField]
        private TownLabelsInstaller.Settings townLabelSettings;
        [SerializeField]
        private RailroadBuilderInstaller.Settings railroadControlsSettings;
        [SerializeField]
        private TrainsManagerInstaller.Settings trainsSettings;
        [SerializeField]
        private BankInstaller.Settings bankSettings;
        [SerializeField]
        private GoodsSettings goodsSettings;
        [SerializeField]
        private GameUI gameUI;
        [SerializeField]
        private MapDebugDrawer mapDebugDrawer;

        public override void InstallBindings()
        {
            Debug.Log(RailroadFsharp.Module.hello("man"));

            Container.BindInstances(
                stationLabelSettings,
                townLabelSettings,
                railroadControlsSettings,
                trainsSettings,
                bankSettings,
                goodsSettings,
                gameUI,
                mapDebugDrawer);

            Container.Bind<BankOfAccount>()
                .FromSubContainerResolve()
                .ByInstaller<BankInstaller>()
                .AsSingle();

            Container.Bind<TownsManager>()
                .AsSingle()
                .NonLazy();

            Container.Bind<RailroadInputBehaviour>()
                .FromComponentInNewPrefab(railroadControlsSettings.railroadControlsInputPrefab)
                .AsSingle()
                .WithArguments(railroadControlsSettings.camera);

            {
                // TODO: move to railroadBuilderInstaller?
                Container.Bind<RailroadBuilderUI>()
                    .AsSingle();
                Container.Bind<StationBehaviour>()
                    .FromInstance(Instantiate(railroadControlsSettings.stationGhostPrefab))
                    .AsSingle();
                Container.Bind<BuilderCursorBehaviour>()
                    .FromInstance(Instantiate(railroadControlsSettings.builderCursorPrefab))
                    .AsSingle();
            }

            Container.Bind<StationManager>()
                .FromSubContainerResolve()
                .ByInstaller<StationManagerInstaller>()
                .AsSingle();

            Container.Bind<TrainsManager>()
                .FromSubContainerResolve()
                .ByNewGameObjectInstaller<TrainsManagerInstaller>() // enable MonoKernel
                .WithGameObjectName("TrainsManagerContext")
                .AsSingle();

            Container.BindInterfacesAndSelfTo<GoodsManager>()
                .AsSingle()
                .NonLazy();

            Container.Bind<GoodsTrader>()
                .AsSingle();

            Container.Bind<RailroadGame>()
                .AsSingle()
                .NonLazy();

            Container.Bind(typeof(StationRoutesManager), typeof(RailSegmentGraph))
                .FromSubContainerResolve()
                .ByInstaller<StationRoutesInstaller>()
                .AsSingle()
                .NonLazy();

            Container.Bind<RailroadBuilder>()
                .FromSubContainerResolve()
                .ByInstaller<RailroadBuilderInstaller>()
                .AsSingle()
                .NonLazy();

            Container.Bind<StationLabelsManager>()
                .FromSubContainerResolve()
                // https://github.com/modesttree/Zenject/issues/160
                .ByNewGameObjectInstaller<StationLabelsInstaller>() // enable MonoKernel
                .WithGameObjectName("StationLabelsContext")
                .AsSingle()
                .NonLazy();

            Container.Bind<TownLabelsManager>()
                .FromSubContainerResolve()
                // https://github.com/modesttree/Zenject/issues/160
                .ByNewGameObjectInstaller<TownLabelsInstaller>() // enable MonoKernel
                .WithGameObjectName("TownLabelsContext")
                .AsSingle()
                .NonLazy();
        }
    }
}