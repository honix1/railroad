﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class RailroadBuilderInstaller :
        Installer<RailroadBuilderInstaller.Settings, RailroadBuilderInstaller>
    {
        [Serializable]
        public class Settings
        {
            public Camera camera;
            public RailroadInputBehaviour railroadControlsInputPrefab;
            public TownBehaviour townPrefab;
            public RailcarBehaviour trainPrefab;
            public GameObject railPiecePrefab;
            public GameObject railPieceBlueprintPrefab;
            public StationBehaviour stationGhostPrefab;
            public BuilderCursorBehaviour builderCursorPrefab;
            public float railPieceLength = 1f;
            public Transform poolContainer;
            public GameObject stationPrefab;
        }

        private readonly Settings settings;

        public RailroadBuilderInstaller(Settings settings)
        {
            this.settings = settings;
        }

        public override void InstallBindings()
        {
            Container.Bind<RailroadBuilder>()
                .AsSingle();

            Container.BindMemoryPool<Transform, RailroadBuilder.RailPool>()
                .FromComponentInNewPrefab(settings.railPieceBlueprintPrefab)
                .UnderTransformGroup("RailsPool")
                .AsCached();

            Container.BindMemoryPool<RailSegment, RailSegmentPool>()
                .AsCached();

            Container.BindMemoryPool<Transform, RailPiecePool>()
                .FromComponentInNewPrefab(settings.railPiecePrefab)
                .AsCached();

            Container.BindMemoryPool<RailSegmentBehaviour, RailSegmentBehaviourPool>()
                .FromNewComponentOnNewGameObject()
                .AsCached();
        }
    }

    public class RailPiecePool : MonoMemoryPool<Transform> { }
    public class RailSegmentBehaviourPool : MonoPoolableMemoryPool<RailSegment, RailSegmentBehaviour> { }

    // TODO: modelToView pattern can be reused with abstract ModelViewPool
    public class RailSegmentPool : PoolableMemoryPool<BezierCurve, RailSegment>
    {
        private readonly Dictionary<RailSegment, RailSegmentBehaviour> modelToView =
            new Dictionary<RailSegment, RailSegmentBehaviour>();

        private readonly RailSegmentBehaviourPool railSegmentBehaviourPool;

        public RailSegmentPool(RailSegmentBehaviourPool railSegmentBehaviourPool)
        {
            this.railSegmentBehaviourPool = railSegmentBehaviourPool;
        }

        protected override void Reinitialize(BezierCurve bezierCurve, RailSegment item)
        {
            base.Reinitialize(bezierCurve, item);

            modelToView[item] = railSegmentBehaviourPool.Spawn(item);
        }

        protected override void OnDespawned(RailSegment item)
        {
            base.OnDespawned(item);

            railSegmentBehaviourPool.Despawn(modelToView[item]);
        }
    }
}