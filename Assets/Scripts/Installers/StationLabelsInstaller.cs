﻿using System;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class StationLabelsInstaller : Installer<StationLabelsInstaller>
    {
        [Serializable]
        public class Settings
        {
            public StationLabelBehaviour stationLabelPrefab;
            public Transform poolContainer;
        }

        private readonly Settings settings;

        public StationLabelsInstaller(Settings settings)
        {
            this.settings = settings;
        }

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<StationLabelsManager>().AsSingle();

            Container.BindMemoryPool<StationLabelBehaviour, StationLabelBehaviour.Pool>()
                .FromComponentInNewPrefab(settings.stationLabelPrefab)
                .UnderTransform(settings.poolContainer)
                .AsSingle();
        }
    }
}