﻿using UnityEngine;
using Zenject;

namespace Railroad
{
    public class StationManagerInstaller : Installer<StationManagerInstaller>
    {
        private readonly RailroadBuilderInstaller.Settings railroadControlsSettings;

        public StationManagerInstaller(RailroadBuilderInstaller.Settings railroadControlsSettings)
        {
            this.railroadControlsSettings = railroadControlsSettings;
        }

        public override void InstallBindings()
        {
            Container.BindFactory<IStationView, IStationView.Factory>()
                .FromComponentInNewPrefab(railroadControlsSettings.stationPrefab)
                .AsSingle();

            Container.BindFactory<Town, RailSegment, float, Station, Station.Factory>()
                .AsSingle();

            Container.Bind<StationManager>()
                .AsSingle();
        }
    }
}