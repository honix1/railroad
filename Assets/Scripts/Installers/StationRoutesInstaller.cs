﻿using System.Collections.Generic;
using Zenject;

namespace Railroad
{
    public class StationRoutesInstaller : Installer<StationRoutesInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<StationRoutesManager>()
                .AsSingle();

            Container.BindInterfacesAndSelfTo<RailSegmentGraph>()
                .AsSingle();

            Container.BindFactory<
                RailSegment,
                float,
                RailSegment,
                float,
                SegmentRoute,
                SegmentRoute.Factory>()
                .AsSingle();

            Container.BindFactory<
                IEnumerable<Station>,
                bool,
                StationRoute,
                StationRoute.Factory>()
                .AsSingle();
        }
    }
}