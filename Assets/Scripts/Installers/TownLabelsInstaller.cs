﻿using System;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class TownLabelsInstaller : Installer<TownLabelsInstaller>
    {
        [Serializable]
        public class Settings
        {
            public TownLabelBehaviour townLabelPrefab;
            public Transform poolContainer;
        }

        private readonly Settings settings;

        public TownLabelsInstaller(Settings settings)
        {
            this.settings = settings;
        }

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<TownLabelsManager>().AsSingle();

            Container.BindMemoryPool<TownLabelBehaviour, TownLabelBehaviour.Pool>()
                .FromComponentInNewPrefab(settings.townLabelPrefab)
                .UnderTransform(settings.poolContainer)
                .AsSingle();
        }
    }
}