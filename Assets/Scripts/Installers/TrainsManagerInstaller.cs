﻿using System;
using System.Collections.Generic;
using Zenject;

namespace Railroad
{
    public class TrainsManagerInstaller : Installer<TrainsManagerInstaller>
    {
        [Serializable]
        public class Settings
        {
            public RailcarBehaviour locomotiveRailcarPrefab;
        }

        private readonly TickableManager tickableManager;
        private readonly GoodsSettings goodsSettings;
        private readonly Settings settings;

        public TrainsManagerInstaller
        (
            TickableManager tickableManager,
            GoodsSettings goodsSettings,
            Settings settings
        )
        {
            this.tickableManager = tickableManager;
            this.goodsSettings = goodsSettings;
            this.settings = settings;
        }

        public override void InstallBindings()
        {
            Container.Bind<GetRailcarBehaviourPool>()
                .FromSubContainerResolve()
                .ByMethod(container =>
                {
                    container.BindMemoryPool<RailcarBehaviour, RailcarBehaviourPool>()
                        .WithId(null)
                        .FromComponentInNewPrefab(settings.locomotiveRailcarPrefab)
                        .AsCached();

                    foreach (Good good in goodsSettings.Goods)
                    {
                        container.BindMemoryPool<RailcarBehaviour, RailcarBehaviourPool>()
                            .WithId(good)
                            .FromComponentInNewPrefab(good.railcarPrefab)
                            .AsCached();
                    }

                    container.Bind<GetRailcarBehaviourPool>()
                        .FromInstance(good => container.TryResolveId<RailcarBehaviourPool>(good));
                })
                .AsSingle();

            Container.BindMemoryPool<Railcar, RailcarPool>()
                .AsCached();

            Container.BindFactory<Train, Train.Factory>()
                .AsSingle()
                .OnInstantiated((context, obj) => tickableManager?.Add(obj as ITickable));

            Container.Bind<TrainsManager>()
                .AsSingle();
        }
    }

    public delegate MonoPoolableMemoryPool<Railcar, RailcarBehaviour> GetRailcarBehaviourPool(Good good);

    public class RailcarBehaviourPool : MonoPoolableMemoryPool<Railcar, RailcarBehaviour> { }

    public class RailcarPool : PoolableMemoryPool<Good, Railcar>
    {
        private readonly Dictionary<Railcar, RailcarBehaviour> modelToView =
            new Dictionary<Railcar, RailcarBehaviour>();

        private readonly GetRailcarBehaviourPool getRailcarViewPool;

        public RailcarPool
        (
            GetRailcarBehaviourPool getRailcarViewPool
        )
        {
            this.getRailcarViewPool = getRailcarViewPool;
        }

        protected override void Reinitialize(Good p1, Railcar item)
        {
            base.Reinitialize(p1, item);

            RailcarBehaviour railcarBehaviour = getRailcarViewPool(item.Good).Spawn(item);
            modelToView[item] = railcarBehaviour;
        }

        protected override void OnDespawned(Railcar item)
        {
            base.OnDespawned(item);

            getRailcarViewPool(item.Good).Despawn(modelToView[item]);
        }
    }
}

