﻿using UnityEngine;
using Zenject;

namespace Railroad
{
    public class MapDebugDrawer : MonoBehaviour
    {
        private RailSegmentGraph graph;

        [Inject]
        public void Initialize(
            RailSegmentGraph graph)
        {
            this.graph = graph;
        }

        private void OnDrawGizmos()
        {
            if (graph != null)
            {
                foreach (DirectedGraphConnection<RailSegment> connection in graph.ReadConnections())
                {
                    Gizmos.color = new Color(1, 1, 1, 0.5f);
                    var from = connection.from.value.GetCenter();
                    var to = connection.to.value.GetCenter();
                    Gizmos.matrix = Matrix4x4.LookAt(from, to, Vector3.up);
                    Gizmos.matrix *= Matrix4x4.Scale(new Vector3(1, 1, (to - from).magnitude));
                    Gizmos.DrawLine(new Vector3(0, 0, 0), new Vector3(0, 0, 1));
                    Gizmos.DrawLine(new Vector3(0, 0, 1), new Vector3(0.5f, 0, 0.9f));
                    Gizmos.DrawLine(new Vector3(0, 0, 1), new Vector3(-0.5f, 0, 0.9f));
                }
            }
        }
    }
}