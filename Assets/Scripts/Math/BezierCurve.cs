﻿using System.Collections.Generic;
using UnityEngine;

namespace Railroad
{
    // https://catlikecoding.com/unity/tutorials/curves-and-splines/
    // http://rsdn.org/?article/multimedia/Bezier.xml

    public struct BezierCurve
    {
        public readonly Vector3 p1;
        public readonly Vector3 p2;
        public readonly Vector3 p3;
        public readonly Vector3 p4;

        private float cachedLength;
        private float[] cachedArcLengthTable;

        public BezierCurve
        (
            Vector3 p1,
            Vector3 p2,
            Vector3 p3,
            Vector3 p4
        )
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;

            cachedLength = -1;
            cachedArcLengthTable = null;
        }

        public Vector3 Sample(float t, bool linearInput = false)
        {
            if (linearInput)
            {
                t = LinearToUnlinearT(t);
            }

            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;
            return
                oneMinusT * oneMinusT * oneMinusT * p1 +
                3f * oneMinusT * oneMinusT * t * p2 +
                3f * oneMinusT * t * t * p3 +
                t * t * t * p4;
        }

        public Vector3 SampleDerivative(float t, bool linearInput = false)
        {
            if (linearInput)
            {
                t = LinearToUnlinearT(t);
            }

            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;
            return
                3f * oneMinusT * oneMinusT * (p2 - p1) +
                6f * oneMinusT * t * (p3 - p2) +
                3f * t * t * (p4 - p3);
        }

        public float LengthNonCurve()
        {
            float result = 0;
            result += Vector3.Distance(p1, p2);
            result += Vector3.Distance(p2, p3);
            result += Vector3.Distance(p3, p4);
            return result;
        }

        public float Length()
        {
            if (cachedLength < 0)
            {
                ValidateCacheArcLengthTable();
                cachedLength = cachedArcLengthTable[cachedArcLengthTable.Length - 1];
            }
            return cachedLength;
        }

        public float LengthAt(float t, bool linearInput = false)
        {
            if (linearInput)
            {
                t = LinearToUnlinearT(t);
            }

            ValidateCacheArcLengthTable();

            float full = t * cachedArcLengthTable.Length;
            int floor = Mathf.FloorToInt(full);
            float mod = full % 1f;

            if (floor < cachedArcLengthTable.Length)
            {
                float length = cachedArcLengthTable[floor];

                if (floor > 0)
                {
                    length += Mathf.Lerp(0, cachedArcLengthTable[floor] - cachedArcLengthTable[floor - 1], mod);
                }

                return length;
            }
            else
            {
                return Length();
            }
        }

        public (BezierCurve, BezierCurve) Divide(float t)
        {
            // http://rsdn.org/article/multimedia/Bezier/bezier06.gif
            Vector3 p12, p23, p34;
            Vector3 p123, p234;
            Vector3 p1234;

            p12 = Vector3.Lerp(p1, p2, t);
            p23 = Vector3.Lerp(p2, p3, t);
            p34 = Vector3.Lerp(p3, p4, t);
            p123 = Vector3.Lerp(p12, p23, t);
            p234 = Vector3.Lerp(p23, p34, t);
            p1234 = Vector3.Lerp(p123, p234, t);

            return (
                new BezierCurve(p1, p12, p123, p1234),
                new BezierCurve(p1234, p234, p34, p4)
            );
        }

        public float ClosestT(Vector3 point, bool linear = false)
        {
            float closestT = 0;
            float closestDistance = float.MaxValue;
            int pieces = (int)Length() * 5;
            for (int i = 1; i < pieces; i++)
            {
                float t = (float)i / pieces;
                Vector3 sample = Sample(t, linear);
                float sampleDistance = Vector3.Distance(sample, point);
                if (sampleDistance < closestDistance)
                {
                    closestDistance = sampleDistance;
                    closestT = t;
                }
            }
            return closestT;
        }

        public Vector3 ClosestPoint(Vector3 point)
        {
            return Sample(ClosestT(point));
        }

        public PositionNormal ClosestPositionNormal(Vector3 point)
        {
            float t = ClosestT(point);
            var normal = SampleDerivative(t).normalized;

            return new PositionNormal()
            {
                position = Sample(t),
                normal = normal
            };
        }

        public PositionNormal SamplePositionNormal(float t, bool linearInput = false)
        {
            var normal = SampleDerivative(t, linearInput).normalized;

            return new PositionNormal()
            {
                position = Sample(t, linearInput),
                normal = normal
            };
        }

        public IEnumerable<Vector3> DiscretePath(float step)
        {
            int pieces = Mathf.RoundToInt(LengthNonCurve() / step);
            for (int i = 0; i < pieces; i++)
            {
                float t = (float)i / pieces;
                yield return Sample(t);
            }
        }

        public IEnumerable<PositionNormal> DiscretePathWithNoramals(float step, bool linear = false)
        {
            int pieces = Mathf.RoundToInt(LengthNonCurve() / step);
            for (int i = 0; i < pieces; i++)
            {
                float t = (float)i / pieces;
                Vector3 normal = SampleDerivative(t, linear).normalized;
                yield return new PositionNormal()
                {
                    position = Sample(t, linear),
                    normal = normal
                };
            }
        }

        public float LinearToUnlinearT(float t)
        {
            t = Mathf.Clamp01(t);

            ValidateCacheArcLengthTable();

            float distance = Length() * t;

            // TODO: binary seach or something faster than O(n)
            // move to extensions
            static int FindMaxLeOrEqThan(float[] array, float t)
            {
                int index = 0;
                for (int i = 1; i < array.Length; i++)
                {
                    index = i;
                    if (array[index] >= t)
                    {
                        return index - 1;
                    }
                }
                return index;
            }

            int index = FindMaxLeOrEqThan(cachedArcLengthTable, distance);

            if (index < 0)
            {
                return 0;
            }
            else if (cachedArcLengthTable[index] == distance)
            {
                return (float)index / (cachedArcLengthTable.Length - 1);
            }
            else
            {
                float a = cachedArcLengthTable[index];
                float b = cachedArcLengthTable[index + 1];
                float d = (distance - a) / (b - a);
                return (index + d) / (cachedArcLengthTable.Length - 1);
            }
        }

        public float UnlinearToLinearT(float t)
        {
            return LengthAt(t) / Length();
        }

        private void ValidateCacheArcLengthTable()
        {
            if (cachedArcLengthTable == null)
            {
                cachedArcLengthTable = CalcArcLengthTable();
            }
        }

        private float[] CalcArcLengthTable()
        {
            const int pieces = 64;
            float[] table = new float[pieces];
            table[0] = 0;
            Vector3 prevSample = Sample(0);
            for (int i = 1; i < pieces; i++)
            {
                Vector3 sample = Sample((float)i / (pieces - 1));
                table[i] = table[i - 1] + Vector3.Distance(prevSample, sample);
                prevSample = sample;
            }
            return table;
        }

        public struct PositionNormal
        {
            public Vector3 position;
            public Vector3 normal;
        }
    }
}