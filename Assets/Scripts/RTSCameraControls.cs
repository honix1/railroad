using UnityEngine;
using UnityEngine.InputSystem;

namespace Railroad
{
    public class RTSCameraControls : MonoBehaviour
    {
        [SerializeField]
        private float dragSpeed = 1f;

        private bool dragging;

        // TODO: split input and camera logic
        public void OnCameraDragStart(InputValue inputValue)
        {
            dragging = true;
        }

        public void OnCameraDragEnd(InputValue inputValue)
        {
            dragging = false;
        }

        public void OnCameraDragMovement(InputValue inputValue)
        {
            //Debug.Log(inputValue);
            if (dragging)
            {
                Vector2 input = inputValue.Get<Vector2>();
                Vector3 move = new Vector3(input.x, 0, input.y);
                move = -move; // drag effect
                move *= dragSpeed;
                transform.position = transform.position + move;
            }
        }
    }
}