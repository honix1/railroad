using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Railroad
{
    public class BuilderCursorBehaviour : MonoBehaviour
    {
        [SerializeField]
        private GameObject middleCursor;
        [SerializeField]
        private GameObject capCursor;

        public void HideCursor()
        {
            middleCursor.SetActive(false);
            capCursor.SetActive(false);
        }

        internal void ShowMiddleCursor(RailSegment railSegment, Vector3 position, bool forward)
        {
            HideCursor();
            middleCursor.SetActive(true);

            var posRot = railSegment.BezierCurve.ClosestPositionNormal(position);
            transform.position = posRot.position;
            transform.forward = posRot.normal * (forward ? 1 : -1);
        }

        internal void ShowCapCursor(RailSegment railSegment, Vector3 position, bool forward)
        {
            HideCursor();
            capCursor.SetActive(true);

            var posRot = railSegment.BezierCurve.ClosestPositionNormal(position);
            transform.position = posRot.position;
            transform.forward = posRot.normal * (forward ? 1 : -1);
        }
    }
}