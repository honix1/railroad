using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Railroad
{
    public class RailroadBuilder
    {
        public enum RailModifyMode
        {
            Start,
            End,
            MiddleForward,
            MiddleBackward
        }

        private readonly List<Vector3> railPoints = new List<Vector3>();

        private RailSegment prevRailSegment;
        private bool prevRailSegmentDir = true;

        private BezierCurve bezierCurve;

        //private int roadNextID = 0;
        private readonly RailSegmentGraph graph;
        private readonly RailPool ghostRailPool;
        private readonly RailSegmentPool railSegmentPool;
        //private readonly RailroadBuilderUI railroadBuilderUI;
        private readonly RailroadBuilderInstaller.Settings settings;

        public RailroadBuilder(
            RailSegmentGraph graph,
            //RailroadBuilderUI railroadBuilderUI,
            RailPool ghostRailPool,
            RailSegmentPool railSegmentPool,
            RailroadBuilderInstaller.Settings settings)
        {
            this.graph = graph;
            //this.railroadBuilderUI = railroadBuilderUI;
            this.ghostRailPool = ghostRailPool;
            this.railSegmentPool = railSegmentPool;
            this.settings = settings;
        }

        public RailModifyMode GetRailModifyMode(RailSegment railSegment, Vector3 position)
        {
            const float limStart = 0.2f;
            const float limEnd = 1 - limStart;
            float t = railSegment.BezierCurve.ClosestT(position);
            if (t < limStart && graph.GetFromConnections(railSegment).FirstOrDefault() == null)
            {
                return RailModifyMode.Start;
            }
            else if (t > limEnd && graph.GetToConnections(railSegment).FirstOrDefault() == null)
            {
                return RailModifyMode.End;
            }
            else
            {
                bool even = t % 0.2f > 0.1f;
                return
                    even ?
                    RailModifyMode.MiddleForward :
                    RailModifyMode.MiddleBackward;
            }
        }

        public void RailSegmentMove(RailSegment railSegment, Vector3 position)
        {
            if (railPoints.Count > 0)
            {
                switch (GetRailModifyMode(railSegment, position))
                {
                    case RailModifyMode.Start:
                        RailConnectCurve(railSegment, start: true);
                        break;
                    case RailModifyMode.End:
                        RailConnectCurve(railSegment, start: false);
                        break;
                    case RailModifyMode.MiddleForward:
                        RailMergeCurve(railSegment, position, forward: true);
                        break;
                    case RailModifyMode.MiddleBackward:
                        RailMergeCurve(railSegment, position, forward: false);
                        break;
                }
                RedrawGhostRail();
            }
        }

        public void RailModify(RailSegment railSegment, Vector3 position)
        {
            var railModifyMode = GetRailModifyMode(railSegment, position);

            if (railPoints.Count > 0) // connecting to other segment
            {
                switch (railModifyMode)
                {
                    case RailModifyMode.Start:
                        RailConnect(railSegment, position);
                        break;
                    case RailModifyMode.End:
                        RailConnect(railSegment, position);
                        break;
                    case RailModifyMode.MiddleForward:
                        RailMerge(railSegment, position, forward: true);
                        break;
                    case RailModifyMode.MiddleBackward:
                        RailMerge(railSegment, position, forward: false);
                        break;
                }
            }
            else // starting new segment
            {
                switch (railModifyMode)
                {
                    case RailModifyMode.Start:
                        railPoints.Add(railSegment.BezierCurve.Sample(1));
                        railPoints.Add(railSegment.BezierCurve.Sample(0));
                        prevRailSegment = railSegment;
                        prevRailSegmentDir = false;
                        break;
                    case RailModifyMode.End:
                        railPoints.Add(railSegment.BezierCurve.Sample(0));
                        railPoints.Add(railSegment.BezierCurve.Sample(1));
                        prevRailSegment = railSegment;
                        prevRailSegmentDir = true;
                        break;
                    case RailModifyMode.MiddleForward:
                        RailBranch(railSegment, position, forward: true);
                        break;
                    case RailModifyMode.MiddleBackward:
                        RailBranch(railSegment, position, forward: false);
                        break;
                }
            }
        }

        public void RailConnect(RailSegment railSegment, Vector3 position)
        {
            float t = railSegment.BezierCurve.ClosestT(position);
            bool start = t < 0.5f;

            RailConnectCurve(railSegment, start);

            RailSegment newRailSegment = BuildRailSegment(bezierCurve);

            graph.AddConnection(newRailSegment, true, railSegment, start);
            graph.AddConnection(railSegment, !start, newRailSegment, false);
        }

        private void RailConnectCurve(RailSegment railSegment, bool start)
        {
            Vector3 connectPoint = railSegment.BezierCurve.Sample(start ? 0 : 1);

            bezierCurve = new BezierCurve(
                bezierCurve.p1,
                bezierCurve.p2,
                -(railSegment.BezierCurve.Sample(start ? 0.25f : 0.75f) - connectPoint) + connectPoint,
                connectPoint
            );
        }

        public void RailMerge(RailSegment railSegment, Vector3 position, bool forward)
        {
            RailMergeCurve(railSegment, position, forward);

            RailSegment temp = prevRailSegment;
            prevRailSegment = null;

            (RailSegment aRailSegment, RailSegment bRailSegment) =
                SplitRailSegment(railSegment, position);

            prevRailSegment = temp;

            RailSegment newRailSegment = BuildRailSegment(bezierCurve);

            if (forward)
            {
                graph.AddConnection(newRailSegment, true, bRailSegment, true);
                graph.AddConnection(bRailSegment, false, newRailSegment, false);
            }
            else
            {
                graph.AddConnection(newRailSegment, true, aRailSegment, false);
                graph.AddConnection(aRailSegment, true, newRailSegment, false);
            }
        }

        private void RailMergeCurve(RailSegment railSegment, Vector3 position, bool forward)
        {
            float t = railSegment.BezierCurve.ClosestT(position);

            float dir = 0.25f * (forward ? 1 : -1);

            bezierCurve = new BezierCurve(
                bezierCurve.p1,
                bezierCurve.p2,
                railSegment.BezierCurve.Sample(t - dir),
                railSegment.BezierCurve.Sample(t)
            );
        }

        public void RailBranch(RailSegment railSegment, Vector3 position, bool forward)
        {
            (RailSegment aRailSegment, RailSegment bRailSegment) =
                SplitRailSegment(railSegment, position);

            prevRailSegment = forward ? aRailSegment : bRailSegment;
            prevRailSegmentDir = forward;

            float dir = 0.1f * (forward ? 1 : -1);

            float t = railSegment.BezierCurve.ClosestT(position);
            railPoints.Add(railSegment.BezierCurve.Sample(t - dir));
            railPoints.Add(railSegment.BezierCurve.Sample(t));
        }

        public void RemoveRailSegment(RailSegment railSegment)
        {
            graph.RemoveConnection(railSegment);

            railSegmentPool.Despawn(railSegment);
        }

        public void GroundClick(Vector3 position)
        {
            if (railPoints.Count > 0)
            {
                GroundMoveCurve(position);
                BuildRailSegment(bezierCurve);
            }
            railPoints.Add(position);
        }

        public void GroundMove(Vector3 position)
        {
            if (railPoints.Count > 0)
            {
                GroundMoveCurve(position);
                RedrawGhostRail();
            }
        }

        public void Cancel()
        {
            railPoints.Clear();
            prevRailSegment = null;

            ghostRailPool.DespawnAll();
        }

        private void GroundMoveCurve(Vector3 position)
        {
            Vector3 p1, p2, p3, p4;
            
            p1 = railPoints[railPoints.Count - 1];

            if (prevRailSegment == null)
            {
                p2 = Vector3.Lerp(bezierCurve.p1, position, 0.33f);
                p3 = Vector3.Lerp(bezierCurve.p1, position, 0.66f);
            }
            else
            {
                if (prevRailSegmentDir)
                {
                    p2 =
                        bezierCurve.p1 +
                        0.5f * Vector3.Distance(bezierCurve.p1, position) *
                        prevRailSegment.BezierCurve.SampleDerivative(1).normalized;
                }
                else
                {
                    p2 =
                        bezierCurve.p1 +
                        0.5f * Vector3.Distance(bezierCurve.p1, position) *
                        -prevRailSegment.BezierCurve.SampleDerivative(0).normalized;
                }
                p3 = (bezierCurve.p2 + position) * 0.5f;
            }

            p4 = position;

            bezierCurve = new BezierCurve(p1, p2, p3, p4);
        }

        private void RedrawGhostRail()
        {
            IEnumerable<BezierCurve.PositionNormal> path =
                bezierCurve.DiscretePathWithNoramals(settings.railPieceLength, linear: true);

            ghostRailPool.DespawnAll();

            foreach (BezierCurve.PositionNormal positionNormal in path)
            {
                Transform transform = ghostRailPool.Spawn();
                transform.SetPositionAndRotation(
                    positionNormal.position,
                    Quaternion.LookRotation(positionNormal.normal));
            }
        }

        // TODO: make public, and refactor calles
        private RailSegment BuildRailSegment(BezierCurve bezierCurve)
        {
            RailSegment railSegment = railSegmentPool.Spawn(bezierCurve);

            if (prevRailSegment != null)
            {
                if (prevRailSegmentDir)
                {
                    graph.AddConnection(prevRailSegment, true, railSegment, true);
                    graph.AddConnection(railSegment, false, prevRailSegment, false);
                }
                else
                {
                    graph.AddConnection(prevRailSegment, false, railSegment, true);
                    graph.AddConnection(railSegment, false, prevRailSegment, true);
                }
            }

            prevRailSegment = railSegment;
            prevRailSegmentDir = true;

            return railSegment;
        }

        private (RailSegment aRailSegment, RailSegment bRailSegment)
            SplitRailSegment(RailSegment railSegment, Vector3 position)
        {
            float railSegmentT = railSegment.BezierCurve.ClosestT(position);
            (BezierCurve aCurve, BezierCurve bCurve) =
                railSegment.BezierCurve.Divide(railSegmentT);

            // ToList will remember the links
            List<DirectedGraphConnection<RailSegment>> fromConnection =
                graph.GetFromConnections(railSegment).ToList();
            List<DirectedGraphConnection<RailSegment>> toConnection =
                graph.GetToConnections(railSegment).ToList();

            prevRailSegment = null;
            RailSegment aRailSegment = BuildRailSegment(aCurve);
            RailSegment bRailSegment = BuildRailSegment(bCurve);

            foreach(IRailSegmentAttachment attachment in railSegment.GetAttachments())
            {
                if (attachment.RailSegmentT <= railSegmentT)
                    attachment.AttachTo(aRailSegment, Mathf.InverseLerp(0, railSegmentT, attachment.RailSegmentT));
                else
                    attachment.AttachTo(bRailSegment, Mathf.InverseLerp(railSegmentT, 1, attachment.RailSegmentT));
            }

            // Restore railSegment connections
            foreach (DirectedGraphConnection<RailSegment> connection in fromConnection)
            {
                graph.AddConnection(
                    connection.from.value,
                    connection.fromDir,
                    aRailSegment,
                    connection.toDir);
                graph.AddConnection(
                    aRailSegment,
                    !connection.toDir,
                    connection.from.value,
                    !connection.fromDir);
            }
            foreach (DirectedGraphConnection<RailSegment> connection in toConnection)
            {
                graph.AddConnection(
                    bRailSegment,
                    connection.fromDir,
                    connection.to.value,
                    connection.toDir);
                graph.AddConnection(
                    connection.to.value,
                    !connection.toDir,
                    bRailSegment,
                    !connection.fromDir);
            }

            RemoveRailSegment(railSegment);

            return (aRailSegment, bRailSegment);
        }

        public class RailPool : TrackingMonoMemoryPool<Transform> { }
    }
}
