﻿using System;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class RailroadBuilderUI
    {
        private readonly RailroadBuilder railroadBuilder;
        private readonly StationBehaviour stationGhost;
        private readonly BuilderCursorBehaviour builderCursor;

        public RailroadBuilderUI(
            RailroadBuilder railroadBuilder,
            StationBehaviour stationGhost,
            BuilderCursorBehaviour builderCursor
        )
        {
            this.railroadBuilder = railroadBuilder;
            this.stationGhost = stationGhost;
            this.builderCursor = builderCursor;

            Initialize();
        }

        private void Initialize()
        {
            stationGhost.gameObject.SetActive(false);    
        }

        public void ShowCursor(RailSegment railSegment, Vector3 position)
        {
            RailroadBuilder.RailModifyMode mode = 
                railroadBuilder.GetRailModifyMode(railSegment, position);

            switch (mode)
            {
                case RailroadBuilder.RailModifyMode.Start:
                    builderCursor.ShowCapCursor(railSegment, position, forward: false);
                    break;
                case RailroadBuilder.RailModifyMode.End:
                    builderCursor.ShowCapCursor(railSegment, position, forward: true);
                    break;
                case RailroadBuilder.RailModifyMode.MiddleForward:
                    builderCursor.ShowMiddleCursor(railSegment, position, forward: true);
                    break;
                case RailroadBuilder.RailModifyMode.MiddleBackward:
                    builderCursor.ShowMiddleCursor(railSegment, position, forward: false);
                    break;
            }
        }

        public void ShowStationGhost(RailSegment railSegment, Vector3 position)
        {
            stationGhost.gameObject.SetActive(true);
            stationGhost.Place(railSegment, railSegment.BezierCurve.ClosestT(position));
        }

        public void HideStationGhost()
        {
            stationGhost.gameObject.SetActive(false);
        }

        public void Clear()
        {
            HideStationGhost();
            builderCursor.HideCursor();
        }
    }

    //public class StationGhostFactory : PlaceholderFactory<StationBehaviour>
    //{
    //}
}