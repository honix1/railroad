﻿using UnityEngine;

namespace Railroad
{
    public class RailroadGame
    {
        private float startTime;

        public RailroadGame()
        {
            Initialize();
        }

        private void Initialize()
        {
            startTime = Time.time;
        }

        public float GetTime()
        {
            return Time.time - startTime;
        }
    }
}