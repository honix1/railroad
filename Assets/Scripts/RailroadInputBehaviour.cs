﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using Zenject;

namespace Railroad
{
    public class RailroadInputBehaviour : MonoBehaviour
    {
        [SerializeField]
        private LayerMask terrainLayer;
        [SerializeField]
        private LayerMask railsLayer;

        private bool uiLock;

        private RailroadBuilder railroadBuilder;
        private StationManager stationManager;
        private RailroadBuilderUI railroadBuilderUI;
        private new Camera camera;

        [Inject]
        public void Initialize(
            RailroadBuilder railroadBuilder,
            StationManager stationManager,
            RailroadBuilderUI railroadBuilderUI,
            Camera camera)
        {
            this.railroadBuilder = railroadBuilder;
            this.stationManager = stationManager;
            this.railroadBuilderUI = railroadBuilderUI;
            this.camera = camera;
        }


        #region InteractionMode
        public enum InteractionMode
        {
            AddRails,
            RemoveRails,
            Trains,
            TrainsSetDestination,
            AddStation,
        }

        private InteractionMode interactionMode = InteractionMode.AddRails;

        public void SetMode(InteractionMode interactionMode)
        {
            this.interactionMode = interactionMode;
        }

        public void SetAddRailsMode()
        {
            interactionMode = InteractionMode.AddRails;
        }

        public void SetRemoveRailsMode()
        {
            interactionMode = InteractionMode.RemoveRails;
        }

        public void AddStationMode()
        {
            interactionMode = InteractionMode.AddStation;
        }
        #endregion

        private void Update()
        {
            uiLock = EventSystem.current.IsPointerOverGameObject();
        }

        #region Input
        public void OnClick()
        {
            if (uiLock)
                return;

            Vector2 pointer = Mouse.current.position.ReadValue();

            Vector3 position;
            if (PointerRailSegmentHit(pointer, out RailSegment railSegment, out position))
            {
                RailSegmentClick(railSegment, position);
            }
            else if (PointerGroudHit(pointer, out position))
            {
                railroadBuilder.GroundClick(position);
            }
        }

        public void OnPointerMove()
        {
            if (uiLock)
                return;

            Vector2 pointer = Mouse.current.position.ReadValue();

            Vector3 position;
            if (PointerRailSegmentHit(pointer, out RailSegment railSegment, out position))
            {
                SetCurrentMouseHover(railSegment);
                RailSegmentMove(railSegment, position);
            }
            else if (PointerGroudHit(pointer, out position))
            {
                SetCurrentMouseHover(null);
                railroadBuilder.GroundMove(position);
            }
        }

        public void OnCancel()
        {
            railroadBuilder.Cancel();

        }
        #endregion

        public object CurrentMouseHover { get; private set; }

        private void SetCurrentMouseHover(object obj)
        {
            if (CurrentMouseHover != obj)
            {
                CurrentMouseHover = obj;
                MouseHoverChange(obj);
            }
        }

        private void MouseHoverChange(object obj)
        {
            railroadBuilderUI.Clear();
        }
        
        private bool PointerGroudHit(Vector2 pointer, out Vector3 position)
        {
            Ray ray = camera.ScreenPointToRay(new Vector3(pointer.x, pointer.y, 0));

            if (Physics.Raycast(ray, out RaycastHit hitInfo, 1000, terrainLayer.value))
            {
                position = hitInfo.point;
                return true;
            }

            position = Vector3.zero;
            return false;
        }

        private bool PointerRailSegmentHit(Vector2 pointer, out RailSegment railSegment, out Vector3 point)
        {
            Ray ray = camera.ScreenPointToRay(new Vector3(pointer.x, pointer.y, 0));

            if (Physics.Raycast(ray, out RaycastHit hitInfo, 1000, railsLayer.value))
            {
                point = hitInfo.point;
                railSegment = hitInfo.transform.parent.GetComponent<RailSegmentBehaviour>().RailSegment;
                return railSegment is RailSegment;
            }

            railSegment = default;
            point = default;
            return false;
        }

        private void RailSegmentClick(RailSegment railSegment, Vector3 position)
        {
            //Debug.Log(railSegment.name);

            switch (interactionMode)
            {
                case InteractionMode.AddRails:
                    railroadBuilder.RailModify(railSegment, position);
                    break;
                case InteractionMode.RemoveRails:
                    railroadBuilder.RemoveRailSegment(railSegment);
                    break;
                case InteractionMode.Trains:
                    interactionMode = InteractionMode.TrainsSetDestination;
                    break;
                case InteractionMode.TrainsSetDestination:
                    interactionMode = InteractionMode.Trains;
                    break;
                case InteractionMode.AddStation:
                    stationManager.AddStation(railSegment, position);
                    break;
            }
        }

        private void RailSegmentMove(RailSegment railSegment, Vector3 position)
        {
            switch (interactionMode)
            {
                case InteractionMode.AddRails:
                    railroadBuilder.RailSegmentMove(railSegment, position);
                    railroadBuilderUI.ShowCursor(railSegment, position);
                    break;
                case InteractionMode.AddStation:
                    railroadBuilderUI.ShowStationGhost(railSegment, position);
                    break;
            }
        }
    }
}
