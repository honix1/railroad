﻿namespace Railroad
{
    public interface IRailSegmentAttachment
    {
        RailSegment RailSegment { get; }
        float RailSegmentT { get; }

        void AttachTo(RailSegment railSegment, float railSegmentT);
    }
}