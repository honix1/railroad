﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class RailSegment : IPoolable<BezierCurve>
    {
        public BezierCurve BezierCurve { get; set; }

        private HashSet<IRailSegmentAttachment> attachments;

        public void OnSpawned(BezierCurve bezierCurve)
        {
            this.BezierCurve = bezierCurve;
        }

        public void OnDespawned()
        {
        }

        public bool AddAttachment(IRailSegmentAttachment attachment)
        {
            ValidateAttachmentsSet();
            return attachments.Add(attachment);
        }

        public IReadOnlyCollection<IRailSegmentAttachment> GetAttachments()
        {
            return attachments as IReadOnlyCollection<IRailSegmentAttachment> ?? Array.Empty<IRailSegmentAttachment>();
        }

        public Vector3 GetCenter()
        {
            return BezierCurve.Sample(0.5f);
        }

        private void ValidateAttachmentsSet()
        {
            if (attachments is null)
            {
                attachments = new HashSet<IRailSegmentAttachment>();
            }
        }
    }
}