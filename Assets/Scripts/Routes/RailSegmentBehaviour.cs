﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class RailSegmentBehaviour : MonoBehaviour, IPoolable<RailSegment>
    {
        public RailSegment RailSegment { get; private set; }

        private readonly List<Transform> railPieces = new List<Transform>();

        private RailPiecePool railPiecePool;
        private RailroadBuilderInstaller.Settings settings;

        [Inject]
        public void Initialize(RailPiecePool railPiecePool, RailroadBuilderInstaller.Settings settings)
        {
            this.railPiecePool = railPiecePool;
            this.settings = settings;
        }

        public void OnSpawned(RailSegment railSegment)
        {
            this.RailSegment = railSegment;

            SpawnPath();
        }

        public void OnDespawned()
        {
            foreach (Transform piece in railPieces)
            {
                railPiecePool.Despawn(piece);
            }

            railPieces.Clear();
        }

        private void SpawnPath()
        {
            IEnumerable<BezierCurve.PositionNormal> path =
                RailSegment.BezierCurve.DiscretePathWithNoramals(settings.railPieceLength, linear: true);

            foreach (BezierCurve.PositionNormal positionNormal in path)
            {
                Transform piece = railPiecePool.Spawn();

                piece.SetPositionAndRotation(
                    positionNormal.position,
                    Quaternion.LookRotation(positionNormal.normal));

                piece.parent = transform;

                railPieces.Add(piece);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(0, 0, 1, 0.5f);
            Gizmos.DrawSphere(RailSegment.BezierCurve.Sample(0), 1f);
            Gizmos.color = new Color(1, 0, 0, 0.5f);
            Gizmos.DrawSphere(RailSegment.BezierCurve.Sample(1), 1f);

            Gizmos.color = new Color(1, 1, 1, 0.5f);
            Gizmos.DrawSphere(RailSegment.BezierCurve.Sample((Time.time * 0.5f) % 1f, linearInput: true), 0.5f);

            GUI.color = Gizmos.color = Color.white;
            Handles.Label(RailSegment.GetCenter(), name);
        }
    }
}