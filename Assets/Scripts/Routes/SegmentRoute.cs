﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class SegmentRoute
    {
        private readonly RailSegment fromSegment;
        private readonly float fromSegmentT = 1;
        private readonly RailSegment toSegment;
        private readonly float toSegmentT = 1;

        private readonly List<DirectedGraphRouteNode<RailSegment>> segmentRoute;

        private float cachedLength = -1;

        public SegmentRoute
        (
            RailSegmentGraph railSegmentGraph,
            RailSegment fromSegment,
            float fromSegmentT,
            RailSegment toSegment,
            float toSegmentT
        )
        {
            this.fromSegment = fromSegment;
            this.fromSegmentT = fromSegmentT;
            this.toSegment = toSegment;
            this.toSegmentT = toSegmentT;

            segmentRoute =
                railSegmentGraph
                    .RouteByValue(
                        fromSegment,
                        toSegment
                    ).ToList();
        }

        public float Length()
        {
            if (cachedLength < 0)
            {
                cachedLength = LengthUncached();
            }
            return cachedLength;
        }

        private float LengthUncached()
        {
            float meters = 0;

            foreach (var railSegment in segmentRoute)
            {
                meters += railSegment.value.BezierCurve.Length();
            }

            meters -= StartShift() + EndShift();

            return meters;
        }

        private float StartShift()
        {
            bool direction = segmentRoute.First().direction;
            float linearT = fromSegment.BezierCurve.UnlinearToLinearT(fromSegmentT);

            return 
                (direction ? linearT : 1 - linearT) *
                fromSegment.BezierCurve.Length();
        }

        private float EndShift()
        {
            bool direction = segmentRoute.Last().direction;
            float linearT = toSegment.BezierCurve.UnlinearToLinearT(toSegmentT);

            return 
                (direction ? 1 - linearT : linearT) *
                toSegment.BezierCurve.Length();
        }

        public bool IsOnRoute(float meters)
        {
            return meters < Length();
        }

        public Matrix4x4 GetMatrixAt(float meters)
        {
            RailSegment currentRailSegment = null;
            bool currentRailSegmentDirection = true;
            float currentSegmentMeters = 0;

            float metersFromStart = meters + StartShift();

            foreach (var railSegment in segmentRoute)
            {
                float nextSegmentMeters = 
                    currentSegmentMeters + railSegment.value.BezierCurve.Length();

                if (nextSegmentMeters > metersFromStart)
                {
                    currentRailSegment = railSegment.value;
                    currentRailSegmentDirection = railSegment.direction;
                    break;
                }

                currentSegmentMeters = nextSegmentMeters;
            }

            if (currentRailSegment != null)
            {
                float railTravel = (metersFromStart - currentSegmentMeters) / currentRailSegment.BezierCurve.Length();
                railTravel = currentRailSegmentDirection ? railTravel : 1 - railTravel;

                var curveDelta = currentRailSegment.BezierCurve.SampleDerivative(railTravel, linearInput: true);
                curveDelta *= currentRailSegmentDirection ? 1 : -1;

                var pos = currentRailSegment.BezierCurve.Sample(railTravel, linearInput: true);

                return Matrix4x4.LookAt(pos, pos + curveDelta.normalized, Vector3.up);
            }

            return Matrix4x4.identity;
        }

        public class Factory :
            PlaceholderFactory<
                RailSegment,
                float,
                RailSegment,
                float,
                SegmentRoute>
        {
        }
    }
}