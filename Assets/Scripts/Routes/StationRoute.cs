﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class StationRoute
    {
        public Station TargetStation { get; private set; }

        private IEnumerator<Station> enumerator;

        private SegmentRoute currentSegmentRoute;

        private readonly SegmentRoute.Factory segmentRouteFactory;
        private readonly IEnumerable<Station> stationsToVisit;
        private readonly bool loop;

        private float travel = 0f;

        public StationRoute(
            SegmentRoute.Factory segmentRouteFactory,
            IEnumerable<Station> stationsToVisit,
            bool loop)
        {
            this.segmentRouteFactory = segmentRouteFactory;
            this.stationsToVisit = stationsToVisit;
            this.loop = loop;
        }

        private void Initialize()
        {
            enumerator = stationsToVisit.GetEnumerator();
            if (enumerator.MoveNext())
            {
                TargetStation = enumerator.Current;
            }
        }

        public void Start()
        {
            Initialize();
            NextStop();
        }

        public Matrix4x4 GetMatrixForRailcart(int number)
        {
            return currentSegmentRoute.GetMatrixAt(travel - number * 5.5f);
        }

        public (float passed, float remain, bool isOnRoute) Move(float meters)
        {
            travel += meters;

            return (travel, currentSegmentRoute.Length() - travel, currentSegmentRoute.IsOnRoute(travel));
        }

        public SegmentRoute NextStop()
        {
            if (enumerator.MoveNext())
            {
                var prevStop = TargetStation;
                TargetStation = enumerator.Current;

                travel = 0;
                currentSegmentRoute =
                    segmentRouteFactory
                    .Create(
                        prevStop.RailSegment,
                        prevStop.RailSegmentT,
                        TargetStation.RailSegment,
                        TargetStation.RailSegmentT
                    );

                return currentSegmentRoute;
            }
            else if (loop)
            {
                enumerator.Reset();
                return NextStop();
            }

            return null;
        }

        public class Factory :
            PlaceholderFactory<
                IEnumerable<Station>,
                bool,
                StationRoute>
        {
        }
    }
}