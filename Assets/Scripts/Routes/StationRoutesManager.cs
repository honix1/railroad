﻿using System.Collections.Generic;
using System.Linq;

namespace Railroad
{
    public class StationRoutesManager
    {
        private readonly List<StationRoute> routes = new List<StationRoute>();
        private readonly List<Station> stationsToVisit = new List<Station>();
        private Station lastStationClicked;

        private readonly StationLabelsManager labels;
        private readonly TrainsManager trainsManager;
        private readonly StationRoute.Factory townRouteFactory;
        private readonly GoodsManager goodsManager;

        public StationRoutesManager
        (
            StationLabelsManager labels,
            TrainsManager trainsManager,
            StationRoute.Factory townRouteFactory,
            GoodsManager goodsManager
        )
        {
            this.labels = labels;
            this.trainsManager = trainsManager;
            this.townRouteFactory = townRouteFactory;
            this.goodsManager = goodsManager;

            Initialize();
        }

        private void Initialize()
        {
            labels.StationClickEvent.AddListener(OnStationClick);
        }

        public IReadOnlyCollection<StationRoute> GetTownRoutes() => routes;

        private void OnStationClick(Station station)
        {
            if (lastStationClicked == station) // double-click
            {
                CreateRoute();
                lastStationClicked = null;
            }
            else
            {
                stationsToVisit.Add(station);
                lastStationClicked = station;
            }
        }

        private void CreateRoute()
        {
            Train train = trainsManager.AddTrain();

            // Temporary
            {
                goodsManager.AddGoods(train, goodsManager.GetGood("mail"), 2);
                goodsManager.AddGoods(train, goodsManager.GetGood("apple"), 3);
                goodsManager.AddGoods(train, goodsManager.GetGood("banana"), 1);
            }

            StationRoute t = townRouteFactory
                .Create(stationsToVisit.ToList(), true);

            train.SetTownRoute(t);

            stationsToVisit.Clear();
        }
    }
}