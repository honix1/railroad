﻿using UnityEngine;
using Zenject;

namespace Railroad
{
    public interface IStationView
    {
        //void SetMatrix(Matrix4x4 matrix);

        // TODO: parent interface ITransformView
        void Place(RailSegment railSegment, float railSegmentT);

        void SetActive(bool active);

        public Vector3 Position { get; }
        public Vector3 LabelPosition { get; }

        public class Factory : PlaceholderFactory<IStationView>
        {
        }
    }
}