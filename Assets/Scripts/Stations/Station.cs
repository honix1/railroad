﻿using UnityEngine;
using Zenject;

namespace Railroad
{
    public class Station : IRailSegmentAttachment
    {
        public Town Town { get; private set; }
        public IStationView View { get; private set; }
        public RailSegment RailSegment { get; private set; }
        public float RailSegmentT { get; private set; }

        private readonly IStationView.Factory factory;

        public Station
        (
            Town town,
            RailSegment railSegment,
            float railSegmentT,
            IStationView.Factory factory
        )
        {
            this.Town = town;
            this.RailSegment = railSegment;
            this.RailSegmentT = railSegmentT;
            this.factory = factory;

            Initialize();
            AttachTo(railSegment, railSegmentT);
        }

        private void Initialize()
        {
            View = factory.Create();
        }

        public void AttachTo(RailSegment railSegment, float railSegmentT)
        {
            this.RailSegment = railSegment;
            this.RailSegmentT = railSegmentT;

            railSegment.AddAttachment(this);

            View.Place(railSegment, railSegmentT);
        }

        public class Factory :
            PlaceholderFactory<Town, RailSegment, float, Station>
        {
        }
    }
}