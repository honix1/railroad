﻿using UnityEngine;
using Zenject;

namespace Railroad
{
    public class StationBehaviour : MonoBehaviour, IStationView
    {
        [SerializeField]
        private Transform centerPoint;

        public Vector3 Position => transform.position;
        public Vector3 LabelPosition => centerPoint.position;

        public void Place(RailSegment railSegment, float railSegmentT)
        {
            BezierCurve.PositionNormal posRot =
                railSegment.BezierCurve.SamplePositionNormal(railSegmentT);

            transform.SetMatrix(
                Matrix4x4.LookAt(
                    posRot.position,
                    posRot.position + Quaternion.Euler(0, -90, 0) * posRot.normal,
                    Vector3.up));
        }

        public void SetActive(bool active)
        {
            SetActive(active);
        }
    }
}