using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Railroad
{
    public class StationLabelBehaviour : MonoBehaviour
    {
        [field: SerializeField]
        public Button Button { get; private set; }

        public UnityEvent<Station> ButtonClickEvent { get; private set; } = new UnityEvent<Station>();

        public Station Station { get; set; }

        public void ButtonClick()
        {
            ButtonClickEvent.Invoke(Station);
        }

        public class Pool : MonoMemoryPool<StationLabelBehaviour>
        {
        }
    }
}
