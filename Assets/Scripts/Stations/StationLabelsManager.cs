﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Railroad
{
    public class StationLabelsManager : ITickable
    {
        public UnityEvent<Station> StationClickEvent { get; private set; } = new UnityEvent<Station>();

        private readonly Dictionary<Station, StationLabelBehaviour> stationToLabel = new Dictionary<Station, StationLabelBehaviour>();

        private readonly StationManager stations;
        private readonly StationLabelBehaviour.Pool labelsPool;

        public StationLabelsManager(
            StationManager stations,
            StationLabelBehaviour.Pool labelsPool)
        {
            this.stations = stations;
            this.labelsPool = labelsPool;
        }

        public void Tick()
        {
            foreach (Station station in stations.GetStations())
            {
                var pos = station.View.LabelPosition;
                var screenPos = Camera.main.WorldToScreenPoint(pos);
                var viewportPos = Camera.main.WorldToViewportPoint(pos);
                var visible = 
                    viewportPos.x > 0 && viewportPos.x < 1 &&
                    viewportPos.y > 0 && viewportPos.y < 1;

                if (visible)
                {
                    if (stationToLabel.TryGetValue(station, out StationLabelBehaviour label))
                    {
                        UpdateTownLabel(label, screenPos);
                    }
                    else
                    {
                        StationLabelBehaviour newLabel = stationToLabel[station] = labelsPool.Spawn();
                        InitTownLabel(newLabel, station);
                        newLabel.ButtonClickEvent.AddListener(StationClick);
                    }
                }
                else
                {
                    if (stationToLabel.TryGetValue(station, out StationLabelBehaviour label))
                    {
                        DeinitTownLabel(label);
                        labelsPool.Despawn(label);
                    }
                    stationToLabel.Remove(station);
                }
            }
        }

        private void InitTownLabel(StationLabelBehaviour label, Station station)
        {
            label.Station = station;
        }

        private void UpdateTownLabel(StationLabelBehaviour label, Vector3 screenPos)
        {
            label.transform.position = screenPos;
        }

        private void DeinitTownLabel(StationLabelBehaviour label)
        {
            label.ButtonClickEvent.RemoveListener(StationClick);
        }

        private void StationClick(Station station)
        {
            StationClickEvent.Invoke(station);
        }
    }
}