﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Railroad
{
    public class StationManager
    {
        private readonly List<Station> stations = new List<Station>();
        public IReadOnlyCollection<Station> GetStations() => stations;

        private readonly TownsManager towns;
        private readonly Station.Factory factory;

        public StationManager
        (
            TownsManager towns,
            Station.Factory factory
        )
        {
            this.towns = towns;
            this.factory = factory;
        }

        public Station AddStation(Town town, RailSegment railSegment, float railSegmentT)
        {
            Station station = factory.Create(town, railSegment, railSegmentT);

            stations.Add(station);

            return station;
        }

        public void AddStation(RailSegment railSegment, Vector3 position)
        {
            Town town = towns
                .GetTowns()
                .OrderBy(x => Vector3.Distance(x.View.transform.position, position))
                .FirstOrDefault();

            if (town != null)
            {
                AddStation(town, railSegment, railSegment.BezierCurve.ClosestT(position));
            }
        }
    }
}
