﻿using NUnit.Framework;
using Railroad;
using UnityEngine;
using Zenject;

namespace RailroadTests
{
    [TestFixture]
    public class BezierCurveTest : ZenjectUnitTestFixture
    {
        [Test]
        public void Length()
        {
            var curve = new BezierCurve(
                Vector3.right * 0f,
                Vector3.right * 0.25f,
                Vector3.right * 0.75f,
                Vector3.right * 1f
                );

            Assert.AreEqual(1, curve.Length());

            curve = new BezierCurve(
                Vector3.right * 0f,
                Vector3.right * 0f,
                Vector3.right * 0f,
                Vector3.right * 1f
                );

            Assert.AreEqual(1, curve.Length());

            curve = new BezierCurve(
                Vector3.right * -1f,
                Vector3.right * 0f,
                Vector3.right * 0f,
                Vector3.right * 1f
                );

            Assert.AreEqual(2, curve.Length(), 1E-3);

            curve = new BezierCurve(
                Vector3.right * -100f,
                Vector3.right * 0f,
                Vector3.right * 0f,
                Vector3.right * 100f
                );

            Assert.AreEqual(200, curve.Length(), 1E-3);
        }

        [Test]
        public void Uniformly()
        {
            var curve = new BezierCurve(
                Vector3.right * 0f,
                Vector3.right * 0f,
                Vector3.right * 1f,
                Vector3.right * 1f
                );

            Assert.AreEqual(0.25f, curve.Sample(0.25f, linearInput: true).x, 1E-3);
            Assert.AreEqual(0.5f, curve.Sample(0.5f, linearInput: true).x, 1E-3);
            Assert.AreEqual(0.75f, curve.Sample(0.75f, linearInput: true).x, 1E-3);
        }
    }
}
