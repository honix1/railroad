﻿using NUnit.Framework;
using Railroad;
using System.Linq;

namespace RailroadTests
{
    public class DirectedGraphTests
    {
        [Test]
        public void SameTest()
        {
            var graph = new DirectedGraph<object>();

            var node = new DirectedGraphNode<object>();

            var result = graph
                .Route(node, node)
                .ToList();

            Assert.IsTrue(result.ToList().Count == 0);
        }
    }
}
