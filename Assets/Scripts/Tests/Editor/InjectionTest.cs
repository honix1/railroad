﻿using NUnit.Framework;
using Railroad;
using Zenject;

namespace RailroadTests
{
    [TestFixture]
    public class InjectionTest : ZenjectUnitTestFixture
    {
        [Test]
        public void AsSingleSameRef()
        {
            Container.Bind<Good>().AsSingle();
            var good = Container.Resolve<Good>();
            good.name = "CAT";
            var good2 = Container.Resolve<Good>();
            Assert.IsTrue(good2.name.Equals("CAT"));
        }

        [Test]
        public void CantResolvePreviousBind()
        {
            Assert.Catch<ZenjectException>(() =>
            {
                var good = Container.Resolve<Good>();
            });
        }
    }
}
