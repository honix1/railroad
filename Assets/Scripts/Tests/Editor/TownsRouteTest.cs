﻿using Moq;
using NUnit.Framework;
using Railroad;
using UnityEngine;
using Zenject;

namespace RailroadTests
{
    [TestFixture]
    public class TownsRouteTest : ZenjectUnitTestFixture
    {
        [Test]
        public void TrainGoesTwoTowns()
        {
            Container.Bind<IGoodsManager>().FromMock();
            Container.BindInterfacesAndSelfTo<RailSegmentGraph>().AsSingle();

            var settings = new RailroadBuilderInstaller.Settings()
            {
                railPieceBlueprintPrefab = new GameObject(),
                railPieceLength = 1.6f
            };

            Container.Bind<RailroadBuilder>()
                .AsSingle()
                .WithArguments(settings);

            Container.BindMemoryPool<Transform, TrackingMonoMemoryPool<Transform>>()
                .FromComponentInNewPrefab(settings.railPieceBlueprintPrefab)
                .UnderTransformGroup("RailsPool")
                .AsSingle();

            var builder = Container.Resolve<RailroadBuilder>();

            builder.GroundClick(Vector3.right * 0f);
            builder.GroundClick(Vector3.right * 1f);
            builder.GroundClick(Vector3.right * 4f);
            builder.GroundClick(Vector3.right * 5f);

            //var train = new GameObject().AddComponent<TrainBehaviour>();

            //var townMock1 = new Mock<ITown>();
            //townMock1.SetupGet(x => x.TownBehaviour).Returns(() =>
            //{
            //    var townBeh = new GameObject().AddComponent<TownBehaviour>();
            //    townBeh.transform.position = Vector3.right * 0f;
            //    return townBeh;
            //});
            //townMock1.Setup(x => x.GetPosition()).Returns(Vector3.right * 0f);
            //var town1 = townMock1.Object;

            //var townMock2 = new Mock<ITown>();
            //townMock2.SetupGet(x => x.TownBehaviour).Returns(() =>
            //{
            //    var townBeh = new GameObject().AddComponent<TownBehaviour>();
            //    townBeh.transform.position = Vector3.right * 5f;
            //    return townBeh;
            //});
            //townMock2.Setup(x => x.GetPosition()).Returns(Vector3.right * 5f);
            //var town2 = townMock2.Object;

            //Container.Bind<TownsRoute>()
            //    .AsSingle()
            //    .WithArguments(
            //        true,
            //        new ITown[] {
            //            town1,
            //            town2 });

            var townsRoute = Container.Resolve<StationRoute>();
            //train.SetTownRoute(townsRoute);

            for (int i = 0; i < 10; i++)
            {
                townsRoute.Move(0.25f);
                //Debug.Log(train.transform.position);
            }
        }
    }
}
