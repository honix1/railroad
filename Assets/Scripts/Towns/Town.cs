﻿using UnityEngine;

namespace Railroad
{
    public class Town : ITrader, IGoodsNotify
    {
        public TownBehaviour View { get; private set; }
        public TownLabelBehaviour Label { get; private set; }

        public string Name => View.name;

        private readonly GoodsManager goodsManager;

        public Town
        (
            TownBehaviour townBehaviour,
            GoodsManager goodsManager
        )
        {
            View = townBehaviour;
            this.goodsManager = goodsManager;
        }

        public Vector3 GetPosition()
        {
            return View.transform.position;
        }

        public void SetLabel(TownLabelBehaviour townLabelBehaviour)
        {
            Label = townLabelBehaviour;

            Label.NameText.text = View.name;
            Label.SetNeeds(goodsManager.GetNeeds(this));
            Label.SetProduces(goodsManager.GetGoods(this));
        }

        public void RemoveLabel()
        {
            Label = null;
        }

        public void GoodsChanged(Good good, float change)
        {
            if (Label != null)
            {
                Label.SetProduces(goodsManager.GetGoods(this));
            }
        }
    }
}