﻿using UnityEditor;
using UnityEngine;

namespace Railroad
{
    public class TownBehaviour : MonoBehaviour
    {
        [SerializeField]
        private Transform centerPoint;

        public Vector3 Position => transform.position;
        public Vector3 LabelPosition => centerPoint.position;

        private void OnDrawGizmos()
        {
            GUI.color = Gizmos.color = Color.white;
            Handles.Label(transform.position, name);
        }
    }
}