using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Railroad
{
    public class TownLabelBehaviour : MonoBehaviour
    {
        [field: SerializeField]
        public Text NameText { get; private set; }

        [SerializeField]
        private RectTransform needsTransform;
        [SerializeField]
        private Image needIconPrefab;

        [SerializeField]
        private RectTransform producesTransform;
        [SerializeField]
        private Image producesIconPrefab;

        public void SetNeeds(IEnumerable<Good> goods)
        {
            // TODO: use pool
            foreach(Transform needIcon in needsTransform)
            {
                Destroy(needIcon.gameObject);
            }

            // TODO: use pool
            foreach(Good good in goods)
            {
                var icon = Instantiate(needIconPrefab, needsTransform);
                icon.sprite = good.icon;
            }
        }

        public void SetProduces(IEnumerable<GoodQuant> goodQuants)
        {
            // TODO: use pool
            foreach (Transform producesIcon in producesTransform)
            {
                Destroy(producesIcon.gameObject);
            }

            // TODO: use pool
            foreach (var goodQuant in goodQuants)
            {
                var icon = Instantiate(producesIconPrefab, producesTransform);
                icon.sprite = goodQuant.good.icon;
                icon.GetComponentInChildren<Text>().text = $"{Mathf.FloorToInt(goodQuant.quant)}"; // TODO: producesIcon class
            }
        }

        public class Pool : MonoMemoryPool<TownLabelBehaviour>
        {
        }
    }
}
