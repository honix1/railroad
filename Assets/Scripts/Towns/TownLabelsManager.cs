﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Railroad
{
    public class TownLabelsManager : ITickable
    {
        private readonly Dictionary<Town, TownLabelBehaviour> townToLabel =
            new Dictionary<Town, TownLabelBehaviour>();

        private readonly TownsManager towns;
        private readonly GoodsManager goodsManager;
        private readonly TownLabelBehaviour.Pool labelsPool;

        public TownLabelsManager(
            TownsManager towns,
            GoodsManager goodsManager,
            TownLabelBehaviour.Pool labelsPool)
        {
            this.towns = towns;
            // TODO: can we bypass this class to give goodsManager ref to TownLabelBehaviour?
            this.goodsManager = goodsManager;
            this.labelsPool = labelsPool;
        }

        public void Tick()
        {
            foreach (Town town in towns.GetTowns())
            {
                var pos = town.View.LabelPosition;
                var screenPos = Camera.main.WorldToScreenPoint(pos);
                var viewportPos = Camera.main.WorldToViewportPoint(pos);
                var visible =
                    viewportPos.x > 0 && viewportPos.x < 1 &&
                    viewportPos.y > 0 && viewportPos.y < 1;

                if (visible)
                {
                    if (townToLabel.TryGetValue(town, out TownLabelBehaviour label))
                    {
                        UpdateTownLabel(town, label, screenPos);
                    }
                    else
                    {
                        TownLabelBehaviour newLabel = townToLabel[town] = labelsPool.Spawn();
                        InitTownLabel(town, newLabel);
                    }
                }
                else
                {
                    if (townToLabel.TryGetValue(town, out TownLabelBehaviour label))
                    {
                        DeinitTownLabel(town, label);
                        labelsPool.Despawn(label);
                    }
                    townToLabel.Remove(town);
                }
            }
        }

        private void InitTownLabel(Town town, TownLabelBehaviour label)
        {
            town.SetLabel(label);
        }

        private void UpdateTownLabel(Town town, TownLabelBehaviour label, Vector3 screenPos)
        {
            label.transform.position = screenPos;
        }

        private void DeinitTownLabel(Town town, TownLabelBehaviour label)
        {
            town.RemoveLabel();
        }
    }
}