﻿using System.Collections.Generic;
using UnityEngine;

namespace Railroad
{
    public class TownsManager
    {
        private readonly List<Town> towns = new List<Town>();

        public IReadOnlyCollection<Town> GetTowns() => towns;

        private readonly GoodsManager goodsManager;

        public TownsManager
        (
            GoodsManager goodsManager
        )
        {
            this.goodsManager = goodsManager;

            Initialize();
        }

        private void Initialize()
        {
            foreach (TownBehaviour townBehaviour in Object.FindObjectsOfType<TownBehaviour>())
            {
                // TODO: use fabric
                Town town = new Town(townBehaviour, goodsManager);
                towns.Add(town);

                // temporary
                {
                    if (Random.value > 0.5)
                        goodsManager.AddNeeds(town, goodsManager.GetGood("mail"));
                    if (Random.value > 0.5)
                        goodsManager.AddNeeds(town, goodsManager.GetGood("apple"));
                    if (Random.value > 0.5)
                        goodsManager.AddNeeds(town, goodsManager.GetGood("banana"));
                    if (Random.value > 0.5)
                        goodsManager.AddNeeds(town, goodsManager.GetGood("toy"));

                    goodsManager.AddDelta(town, goodsManager.GetGood("mail"), Random.Range(0f, 1f));
                }
            }
        }
    }
}