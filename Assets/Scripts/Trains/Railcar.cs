﻿using System;
using UnityEngine;
using Zenject;

namespace Railroad
{
    public class Railcar : IPoolable<Good>
    {
        public event Action<Matrix4x4> MatrixUpdateEvent;

        public Good Good { get; private set; }
        public Matrix4x4 Matrix { get; private set; }

        public void SetMatrix(Matrix4x4 matrix)
        {
            this.Matrix = matrix;
            MatrixUpdateEvent(matrix);
        }

        public void OnSpawned(Good p1)
        {
            this.Good = p1;
        }

        public void OnDespawned()
        {
        }
    }
}