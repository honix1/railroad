﻿using UnityEngine;
using Zenject;

namespace Railroad
{
    public class RailcarBehaviour : MonoBehaviour, IPoolable<Railcar>
    {
        private Railcar model;

        public void SetMatrix(Matrix4x4 matrix)
        {
            transform.SetMatrix(matrix);
        }

        public void OnSpawned(Railcar p1)
        {
            model = p1;
            model.MatrixUpdateEvent += SetMatrix;
        }

        public void OnDespawned()
        {
            model.MatrixUpdateEvent -= SetMatrix;
        }
    }
}