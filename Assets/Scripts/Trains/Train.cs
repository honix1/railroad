﻿using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Railroad
{
    public class Train : ITickable, IGoodsNotify, ITrader
    {
        public UnityEvent<Train, Station> StopEvent { get; private set; }
            = new UnityEvent<Train, Station>();

        public string Name => "Train-42";

        public IReadOnlyCollection<Railcar> Railcars => railcars;

        private StationRoute stationRoute;

        private bool running = true;
        private float speed;

        private LinkedList<Railcar> railcars = new LinkedList<Railcar>();
        private float maxSpeed = 10f;

        private readonly RailcarPool railcarPool;

        public Train
        (
            RailcarPool railcarPool
        )
        {
            this.railcarPool = railcarPool;

            Initialize();
        }

        private void Initialize()
        {
            Railcar railcar = railcarPool.Spawn(null); // locomotive railcar
            railcars.AddLast(railcar);
        }

        public void SetTownRoute(StationRoute townRoute)
        {
            this.stationRoute = townRoute;
            this.stationRoute.Start();
        }

        public void Tick()
        {
            if (running)
            {
                (float passed, float remain, bool isOnRoute) =
                    stationRoute.Move(Time.deltaTime * speed);

                if (isOnRoute)
                {
                    speed = Mathf.Clamp01(Mathf.Min(passed, remain) / 5) * maxSpeed;
                    speed = Mathf.Clamp(speed, 0.1f, maxSpeed);
                }
                else
                {
                    running = false;

                    Sequence seq = DOTween.Sequence();
                    seq.AppendInterval(1);
                    seq.AppendCallback(() =>
                    {
                        StopEvent.Invoke(this, stationRoute.TargetStation);
                    });
                    seq.AppendInterval(1);
                    seq.AppendCallback(() =>
                    {
                        stationRoute.NextStop();
                        running = true;
                    });
                }
            }

            int i = 0;
            foreach (Railcar railcar in railcars)
            {
                railcar.SetMatrix(stationRoute.GetMatrixForRailcart(i++));
            }
        }

        public void GoodsChanged(Good good, float change)
        {
            if (change > 0)
            {
                for (int i = 0; i < (int)change; i++)
                {
                    Railcar railcar = railcarPool.Spawn(good);
                    railcars.AddLast(railcar);
                }
            }
            else if (change < 0)
            {
                var toRemove = railcars
                    .Reverse()
                    .Where(x => x.Good == good)
                    .Take(-(int)change);

                foreach (Railcar railcar in toRemove)
                {
                    railcars.Remove(railcar);
                    railcarPool.Despawn(railcar);
                }
            }
        }

        public class Factory :
            PlaceholderFactory<Train>
        {
        }
    }
}