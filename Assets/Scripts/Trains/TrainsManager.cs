﻿using System;
using System.Collections.Generic;

namespace Railroad
{
    public class TrainsManager
    {
        private readonly List<Train> trains = new List<Train>();
        public IReadOnlyCollection<Train> GetTrains() => trains;

        private readonly Train.Factory trainFactory;
        private readonly GoodsTrader goodsTrader;

        public TrainsManager
        (
            Train.Factory trainFactory, 
            GoodsTrader goodsTrader
        )
        {
            this.trainFactory = trainFactory;
            this.goodsTrader = goodsTrader;
        }

        public Train AddTrain()
        {
            Train train = trainFactory.Create();
            trains.Add(train);

            train.StopEvent.AddListener(TrainStop);

            return train;
        }

        private void TrainStop(Train arg0, Station arg1)
        {
            goodsTrader.TrySellAll(arg0, arg1.Town);
        }
    }
}